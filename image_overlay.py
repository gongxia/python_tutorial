from PIL import Image
import numpy as np

empty_layer = np.zeros((1386, 1386))
full_layer = np.ones((1386, 1386))*255

color_im = Image.open('color.png')
color_arr = np.array(color_im)

gray_im = Image.open('grayvalue.png')
gray_arr = np.array(gray_im)

gray_layer = np.transpose(gray_arr[:, :, 0])

r = np.transpose(color_arr[:, :, 0])
g = np.transpose(color_arr[:, :, 1])
b = np.transpose(color_arr[:, :, 2])


masked = np.copy(r)
masked[masked == 2] = 0
masked[masked != 0] = 1

invert_masked = np.invert(masked)/255

processed_layers =np.transpose(np.array([r*masked + invert_masked*gray_layer, g*masked+invert_masked*gray_layer , invert_masked*gray_layer],  dtype=np.uint8))

# Convert to image
img = Image.fromarray(processed_layers, "RGB")
img.save('demo.png')