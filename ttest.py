import dicom
import os
import matplotlib.pyplot as plt
from pylab import show, get_cmap
from PIL import Image
import numpy 
from scipy import stats

print "enter folder name"
Foldername = str(raw_input())
PathDicom = "./" + Foldername
lsFiles = []

for dirName, subdirList, fileList in os.walk(PathDicom):
	for filename in fileList:
		lsFiles.append(os.path.join(dirName,filename))

refDs = dicom.read_file(lsFiles[0])
constPixelDims = (int(refDs.Rows),int(refDs.Columns),len(lsFiles))
ArrayDicom = numpy.zeros(constPixelDims, dtype = refDs.pixel_array.dtype)

for filename in lsFiles:
	ds = dicom.read_file(filename)
	ArrayDicom[:,:, lsFiles.index(filename)] = ds.pixel_array

newConstPixelDims = (int(refDs.Rows),int(refDs.Columns),(len(lsFiles)/2))
restArray = numpy. zeros(newConstPixelDims)
activatedArray = numpy.zeros(newConstPixelDims)
newPixelArray = (int(refDs.Rows), int(refDs.Columns))
restPixelIntensityMean = numpy.zeros(newPixelArray)
n = 0
m = 0

# sort the rest and activated image groups
for filename in lsFiles:
	fileOrder = lsFiles.index(filename)
	ds = dicom.read_file(filename)
	if ((fileOrder / 15) % 2) == 0:
		restArray[:,:,n] = ds.pixel_array
		n = n + 1
	else:
		activatedArray[:,:,m] = ds.pixel_array
		m = m + 1

t,p = stats.ttest_rel(activatedArray,restArray,axis = 2)
t[numpy.isnan(t)] = 0

print p
#generate background image
for x in range(int(refDs.Rows)):
	for y in range(int(refDs.Columns)):
		restPixelIntensityMean[x,y] = numpy.mean(restArray[x,y,:])

print "size of p is:"
print p.shape

print "size of t is:"
print t.shape

print "size of restPixelIntensityMean"
print restPixelIntensityMean.shape

# use p value to generate a mask
mask = p
mask[mask >= 0.04] = 0
mask[mask != 0] = 1


plt.imshow(t*mask)
im = plt.gcf()
im.savefig('color.png')
plt.imshow(restPixelIntensityMean, cmap = 'Greys_r')
im2 = plt.gcf()
im2.savefig('grayvalue.png')


