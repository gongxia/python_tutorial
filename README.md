# Tutorial to setup environment for Python for Image Processing


## Using git
- Please refer to github official documentation: https://help.github.com/articles/set-up-git/
- After install git and setup username, password and email, you'll easily get access to all the open access project in Github and Bitbucket
- Just use the following syntax to "clone" the code from Github/Bitbucket to local:
    * ```git clone <repo>```
    * ```Example: git clone https://github.com/fbessho/PyPoi.git```


## Install python
- If you're using Mac, Python 2.7 should already been installed by default
- We'll need the package management tool for Python which called "pip", install pip on mac using this command ```sudo easy_install pip```
- Update the pip to the latest version by using: ```sudo -H pip install --upgrade pip```
- Then use the following command to input all the images processing require packages from Python
    * ```sudo -H pip install numpy```
    * ```sudo -H pip install pillow```
    * ```sudo -H pip install scipy```
    * ```sudo -H pip install nose```
    * ```sudo -H pip install pyamg```

- Then all the package installation should be ready to go.


## Run basic demo examples from open project on Github
- Go to any directory at your computer using commandline
- For example, go to Desktop using: ```cd ~/Desktop```
- Clone the following project from Github by typing: ```git clone https://github.com/parosky/poissonblending.git```
- Then after cloning, you'll notice on your Desktop with a new folder called "poissonblending"
- Go to the "testimages" folder, delete the image "test1-ret.png"
- Then you can try run the code either of the following ways:
    * If you're using Sublime, open the file "poissonblending.py", use "Cmd"+"B" to run (for Mac)
    * Or you can use commandline to go to the folder where "poissonblendding.py" located, then type ```python poissonblendding.py```
    * After finish running, you'll see in the the "testimages" folder, "test1-ret.png" is generated from the Python process


## Run a Python based GUI tool to apply poisson blending
- The source code and specific instruction are at here: https://github.com/fbessho/PyPoi
- Quick Start, just type ```sudo -H pip install pypoi```
- After install it, type ```pypoi``` in commandline, then you can play with it